package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.DBCursor;
import hw5.Document;

class CursorTester {

	/*
	 *Queries:
	 * 	Find all (done?)
	 * 	Find with relational select
	 * 	Find with projection
	 * 	Conditional operators
	 * 	Embedded Documents and arrays
	 */

	@Test
	public void testFindAll() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		DBCursor results = test.find();
		assertTrue(results.count() == 3);
		assertTrue(results.hasNext());
		JsonObject d1 = results.next(); //verify contents?
		assertTrue(results.hasNext());
		JsonObject d2 = results.next();//verify contents?
		assertTrue(results.hasNext());
		JsonObject d3 = results.next();//verify contents?
		assertTrue(!results.hasNext());
	}
	
	@Test
	public void testFindWithProject() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test2");
		String queryString = "{\"item\": {\"name\": \"ab\", \"code\": \"123\"}}";
		JsonObject queryObject = Document.parse(queryString);
		String fieldString = "{\"item\" : 1}";
		JsonObject fieldObject = Document.parse(fieldString);
		DBCursor results = test.find(queryObject, fieldObject);
		assertTrue(results.count() == 2);
		assertTrue(results.hasNext());
		JsonObject d1 = results.next(); //verify contents?
		assertTrue(results.hasNext());
		JsonObject d2 = results.next();//verify contents?
		assertTrue(!results.hasNext());
	}
	
//	TODO
	@Test
	public void testFindWithSelect() {
		DB db = new DB("data");
		
		DBCollection test = db.getCollection("test");
		JsonObject t1 = test.getDocument(0);
		DBCursor results = test.find(t1);
		assertTrue(results.count() == 1);
		assertTrue(results.hasNext());
		JsonObject d1 = results.next(); //verify contents?
		assertTrue(!results.hasNext());
		
		JsonObject t2 = test.getDocument(1);
		DBCursor results2 = test.find(t2);
		assertTrue(results2.count() == 1);
		assertTrue(results2.hasNext());
		JsonObject d2 = results2.next(); //verify contents?
		assertTrue(!results2.hasNext());
		
		JsonObject t3 = test.getDocument(2);
		DBCursor results3 = test.find(t3);
		assertTrue(results3.count() == 1);
		assertTrue(results3.hasNext());
		JsonObject d3 = results3.next(); //verify contents?
		assertTrue(!results.hasNext());
	}
	
	
	
	
	
//	TODO
	@Test
	public void testEmbeddedDocumentAndArrays() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test2");
		String tempString = "{\"item\": {\"name\": \"ab\", \"code\": \"123\"}}";
		JsonObject j1 = Document.parse(tempString);
		DBCursor results = test.find(j1);
		assertTrue(results.count() == 2);
		String tempString2 = "{\"tags\": [ \"A\", \"B\", \"C\"]}";
		JsonObject j2 = Document.parse(tempString2);
		DBCursor results2 = test.find(j2);
		assertTrue(results2.count() == 2);
	}
	
	
	@Test
	public void testEmbeddedDocumentAndArrays2() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		
		String str1 = "{\"embedded.key1.key2\": \"value2\" }";
		JsonObject query1 = Document.parse(str1);
		
		System.out.println("------query"+query1);
		
		DBCursor results = test.find(query1);
		System.out.println(results.count());

		assertTrue(results.count() == 1);

	}
	
	
	
	
	
	
//	yang
	@Test
	public void testConditionalOperatorWithField() {
		DB db = new DB("data");
		
		DBCollection testCollection = db.getCollection("operatorTestData");
//		DBCollection testCollection = db.getCollection("test");
		JsonObject primitive = testCollection.getDocument(0);
		System.out.println(primitive.toString());
		
		String str = "{\"cast\": [ \"qty\", \"tags\", \"three\" ]}";
		JsonObject fields1 = Document.parse(str);
		
		
		String queryStr1 = "{ qty: { $eq: 20 } }";
		JsonObject query1 = Document.parse(queryStr1);
		DBCursor cursor1 = new DBCursor(testCollection, query1, fields1);
//		System.out.println(cursor1.count());
//		while (cursor1.hasNext()) {
//			JsonObject o = cursor1.next();
//			System.out.println("result " + o);
//		}
		assertTrue(cursor1.count() == 2);
				
		
		
		String str2 = "{\"cast\": \"qty\" }";
		JsonObject fields2 = Document.parse(str2);

		String queryStr2 = "{ qty: { $gt: 20 } }";
		JsonObject query2 = Document.parse(queryStr2);
		DBCursor cursor2 = new DBCursor(testCollection, query2, fields2);
		while (cursor2.hasNext()) {
			JsonObject o = cursor2.next();
			System.out.println("result " + o);
		}
		assertTrue(cursor2.count() == 2);

	}
	
//	TODO
	@Test
	public void testConditionalOperator() {
		DB db = new DB("data");
		
		DBCollection testCollection = db.getCollection("operatorTestData");
//		DBCollection testCollection = db.getCollection("test");
		JsonObject primitive = testCollection.getDocument(0);
		System.out.println(primitive.toString());
		
		String queryStr1 = "{ qty: { $eq: 20 } }";
		JsonObject query1 = Document.parse(queryStr1);
		DBCursor cursor1 = new DBCursor(testCollection, query1, null);
//		System.out.println(cursor1.count());

		assertTrue(cursor1.count() == 2);
				
		String queryStr2 = "{ qty: { $gt: 20 } }";
		JsonObject query2 = Document.parse(queryStr2);
		DBCursor cursor2 = new DBCursor(testCollection, query2, null);
		assertTrue(cursor2.count() == 2);
		
		String queryStr3 = "{ qty: { $gte: 20 } }";
		JsonObject query3 = Document.parse(queryStr3);
		DBCursor cursor3 = new DBCursor(testCollection, query3, null);
		assertTrue(cursor3.count() == 4);

		
		String queryStr4 = "{ qty: { $lte: 20 } }";
		JsonObject query4 = Document.parse(queryStr4);
		DBCursor cursor4 = new DBCursor(testCollection, query4, null);
		assertTrue(cursor4.count() == 3);
		
		String queryStr5 = "{ qty: { $ne: 20 } }";
		JsonObject query5 = Document.parse(queryStr5);
		DBCursor cursor5 = new DBCursor(testCollection, query5, null);
		assertTrue(cursor5.count() == 3);

		String queryStr6 = "{ qty: { $in: [ 25, 15 ] } }";
		JsonObject query6 = Document.parse(queryStr6);
		System.out.println("is");
		System.out.println(query6);
		
		DBCursor cursor6 = new DBCursor(testCollection, query6, null);
		assertTrue(cursor6.count() == 2);
		
		String queryStr7 = "{ qty: { $nin: [ 30, 1 ] } }";
		JsonObject query7 = Document.parse(queryStr7);
		DBCursor cursor7 = new DBCursor(testCollection, query7, null);
		assertTrue(cursor7.count() == 4);
		
		String queryStr9 = "{ qty: { $gte: 21 }, _id: {  $gte: 2 }}";
		JsonObject query9 = Document.parse(queryStr9);
		DBCursor cursor9 = new DBCursor(testCollection, query9, null);
//		System.out.println(cursor9.count());
		assertTrue(cursor9.count() == 2);
		
//		failing
//		String queryStr8 = "{ qty: { $gt: 20 }, qty: { $lt: 30 } }";
//		JsonObject query8 = Document.parse(queryStr8);
//		DBCursor cursor8 = new DBCursor(testCollection, query8, null);
//		System.out.println(cursor8.count());
//
//		assertTrue(cursor8.count() == 1);
		
	}
	
	
	
	
	
}
