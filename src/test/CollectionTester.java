package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.DBCursor;
import hw5.Document;

class CollectionTester {

	/*
	 * Things to be tested:
	 * 
	 * Document access (done?)
	 * Document insert/update/delete
	 */

	/*@BeforeEach
	public void setup() throws IOException {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		File inputFile = test.getCollection();
		File tempFile = new File("myTempFile.txt");

		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
		String currentLine;
		boolean stop = false;
		while((currentLine = reader.readLine()) != null) {
		    if(currentLine.equals("\t")) {
		    	writer.write("\n");
		    	writer.write("\t");
		    	writer.write("\n");
		    	continue;
		    }
			writer.write(currentLine);
		}
			    
			    //writer.flush();
			
		
		writer.close(); 
		reader.close(); 
	}
//  original
	*/
	@BeforeEach
	public void setup() {
		try {
			Files.copy(new File("testfiles/data/test.json.dat").toPath(), new File("testfiles/data/test.json").toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("unable to copy files");
			e.printStackTrace();
		}
	}
	@Test
	public void testGetDocument() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		JsonObject primitive = test.getDocument(0);
		assertTrue(primitive.getAsJsonPrimitive("key").getAsString().equals("value"));
	}
	
	
//	TODO
	@Test
	public void testInsert() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");

		String a = "{ \"a\": \"avalue\" }";
		JsonObject obj1 = Document.parse(a);
		String b = "{ \"b\": \"bvalue\" }";
		JsonObject obj2 = Document.parse(b);
		String c = "{ \"c\": \"cvalue\" }";
		JsonObject obj3 = Document.parse(c);
		System.out.print(Document.toJsonString(obj1));
		
		test.insert(obj1);
		test.insert(obj2);
		test.insert(obj3);

		JsonObject res1 = test.getDocument(3);
		JsonObject res2 = test.getDocument(4);
		JsonObject res3 = test.getDocument(5);

		assertTrue(res1.getAsJsonPrimitive("a").getAsString().equals("avalue"));
		assertTrue(res2.getAsJsonPrimitive("b").getAsString().equals("bvalue"));
		assertTrue(res3.getAsJsonPrimitive("c").getAsString().equals("cvalue"));

	}
	
//	TODO
	@Test
	public void testUpdate() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		//test.update(query, update, multi);
		JsonObject t1 = test.getDocument(2);
		String c = "{ \"a\": \"avalue1\" }";
		JsonObject t2 = Document.parse(c);
		try {
			test.update(t1, t2, false);
		} catch (IOException e) {
			e.printStackTrace();
		}
		JsonObject primitive = test.getDocument(2);
		assertTrue(primitive.getAsJsonPrimitive("a").getAsString().equals("avalue1"));
	}
	
	
	
	
//	TODO
	@Test
	public void testRemove() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		JsonObject t1= test.getDocument(0);
		try {
			test.remove(t1, false);
		} catch (IOException e) {
			e.printStackTrace();
		}
		JsonObject t2 = test.getDocument(0);
		assertTrue(test.count() == 2);
	}
	
	@Test void testDrop() {
		DB db = new DB("data");
		String name = "testDrop";
		DBCollection test = db.getCollection("testDrop");
		File[] files = db.getPath().listFiles();
		test.drop();
		
		boolean found = false;
		for (int i = 0; i < files.length; i++) {
			  if (files[i].isFile()) {
			    System.out.println("File " + files[i].getName());
			    System.out.println("looking for " + name);
			    if (files[i].getName().equals(name + ".json")) {
			    	found = true;
				    System.out.println("match " );
				    //return collections.get(name);
			    }	    
			  } 
		}
		
		assertTrue(!found);
	}
//	TODO
	@Test
	public void testDelete() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		JsonObject primitive = test.getDocument(0);
		assertTrue(primitive.getAsJsonPrimitive("key").getAsString().equals("value"));
	}
	
	/*@AfterEach
	public void end() throws IOException {		
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		File inputFile = test.getCollection();
		File tempFile = new File("myTempFile.txt");

		BufferedReader reader = new BufferedReader(new FileReader(tempFile));
		BufferedWriter writer = new BufferedWriter(new FileWriter(inputFile));
		String currentLine;
		while((currentLine = reader.readLine()) != null) {
		    // trim newline when comparing with lineToRemove
			if(currentLine.equals("\t")) {
		    	writer.write("\n");
		    	writer.write("\t");
		    	writer.write("\n");
		    	continue;
		    }
			writer.write(currentLine);
		}
			    
			    //writer.flush();
			
		
		writer.close(); 
		reader.close(); 
		
	}*/
	

}
