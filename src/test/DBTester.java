package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import hw5.DB;
import hw5.DBCollection;

class DBTester {

	/*
	 * Things to consider testing:
	 * 
	 * Properly returns collections
	 * Properly creates new collection
	 * Properly creates new DB (done)
	 * Properly drops db
	 */
	@Test
	void testCreateNewDB() {
		DB hw5 = new DB("hw5");
		assertTrue(new File("testfiles/hw5").exists());
	}
	
	
	//TODO
	@Test
	void testCreateCollection() {
		DB hw5 = new DB("hw5");
		assertFalse(new File("testfiles/hw5/test").exists());
		try
		{
			DBCollection collection = new DBCollection(hw5, "test");
		    Thread.sleep(1000);
			assertTrue(new File("testfiles/hw5/test.json").exists());

		}
		catch (Exception e) {
			
		}
	}
	
	
	//TODO
	@Test
	void testGetCollection() {
		DB hw5 = new DB("hw5");
		
		try {
			DBCollection collection = hw5.getCollection("test");
			assertTrue(new File("testfiles/hw5/test.json").exists());
			
		} catch (Exception e) {
			assertTrue(false);
			System.out.println("did not return collection");
		}
	}
	
	
	
	//TODO
	@Test
	void testDropDB() {
		DB hw5 = new DB("hw5");
		try {
			hw5.dropDatabase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertFalse(new File("testfiles/hw5").exists());
	}
	
	

}
