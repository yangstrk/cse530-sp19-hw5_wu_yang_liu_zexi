package hw5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import com.google.gson.JsonObject;


public class DBCollection {

	/**
	 * Constructs a collection for the given database with the given name. If that
	 * collection doesn't exist it will be created.
	 */

	private String collectionName;
	//private ArrayList<JsonObject> documents;
	private ArrayList<Integer> header;
	private File collectionFile;
	//private Integer increment;
	private String filePath;

	public DBCollection(DB database, String name) {
		// path of collection
		
		File DBDir = database.getPath();
		this.filePath = DBDir + "/" + name + ".json";
		boolean found = false;
		
		try {
		    File[] files = DBDir.listFiles();
		    for (File entry : files) {
		    	// if found, read data from it
		    	if (entry.getName().equals(name + ".json")) {
		    		found = true;
			    	System.out.println("found "+ entry.getName());
			    	this.collectionFile = entry;
			    	//this.loadHeader();
		    	}
		    }
			// if there is no such file, create one
			if (!found) {
				File newFile = new File(filePath);
				if (newFile.createNewFile())  {
				    System.out.println("File is created!");
			    	this.collectionFile = newFile;
			    	this.header = new ArrayList<>();
				} else {
				    System.out.println("File already exists.");
				}
			}
		} catch (Exception e){
			
		}		
		

		
	}

	/**
	 * Returns a cursor for all of the documents in this collection.
	 */
	public DBCursor find() {
		DBCursor cursor = new DBCursor(this,null, null);
		return cursor;
	}

	/**
	 * Finds documents that match the given query parameters.
	 * 
	 * @param query relational select
	 * @return
	 */
	public DBCursor find(JsonObject query) {
		
		DBCursor cursor = new DBCursor(this, query, null);
		return cursor;
	}

	/**
	 * Finds documents that match the given query parameters.
	 * could be tricky
	 * @param query      relational select
	 * @param projection relational project
	 * @return
	 */ 
	public DBCursor find(JsonObject query, JsonObject projection) {
		
		DBCursor cursor = new DBCursor(this, query, projection);
		return cursor;
	}

	// getter of collection file
	public File getCollection() {
		return this.collectionFile;
	}
	
	
	// load the header, only once when reading the file
	private void loadHeader() {
		// get a cursor of this whole collection
		DBCursor cursor = this.find();
		int i=0;		
		while (cursor.hasNext()) {
			this.header.set(i, 1);
			i++;
		}
	}
	
	
	// helper func
	private Integer getAvailableSlot() {
		if(this.header == null) {
			return 0;
		}
		for (int i=0; i<this.header.size();i++) {
			if (this.header.get(i).equals(0)) {  return i;  }
		}
		return header.size();
	}
	
	/**
	 * Inserts documents into the collection Must create and set a proper id before
	 * insertion When this method is completed, the documents should be permanently
	 * stored on disk.
	 * 
	 * @param documents
	 */
	public void insert(JsonObject... documents) {
		for(JsonObject obj : documents) {
			Integer insertLocation = this.getAvailableSlot();
			
			FileWriter file;
			try {
				file = new FileWriter(collectionFile, true);
				file.write(obj.toString());
				file.write("\n");
				file.write("\t");
				file.write("\n");
				file.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	/**
	 * Locates one or more documents and replaces them with the update document.
	 * 
	 * @param query  relational select for documents to be updated
	 * @param update the document to be used for the update
	 * @param multi  true if all matching documents should be updated false if only
	 *               the first matching document should be updated
	 * @throws IOException 
	 */
	public void update(JsonObject query, JsonObject update, boolean multi) throws IOException {
		File inputFile = collectionFile;
		File tempFile = new File("myTempFile.txt");

		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

		String lineToRemove = Document.toJsonString(query);
		String lineToAdd = Document.toJsonString(update);
		String currentLine;
		boolean stop = false;
			while((currentLine = reader.readLine()) != null) {
			    // trim newline when comparing with lineToRemove
			    String trimmedLine = currentLine.trim();
			    trimmedLine = trimmedLine.replaceAll("\\s+", "");
			    if(trimmedLine.equals("")) {
			    	continue;
			    }
			    if(trimmedLine.equals(lineToRemove) && !stop) {
			    	writer.write(lineToAdd);
		    		writer.write("\n");
				    writer.write("\t");
				    writer.write("\n");
			    	if(multi) {
			    		continue;
			    	} else {
			    		stop = true;
			    		continue;
			    	}
			    }
			    writer.write(currentLine);
			    writer.write("\n");
			    writer.write("\t");
			    writer.write("\n");
			    //writer.flush();
			}
		
		writer.close(); 
		reader.close(); 
		boolean successful = tempFile.renameTo(inputFile);
	}

	/**
	 * Removes one or more documents that match the given query parameters
	 * 
	 * @param query relational select for documents to be removed
	 * @param multi true if all matching documents should be updated false if only
	 *              the first matching document should be updated
	 * @throws IOException 
	 */
	public void remove(JsonObject query, boolean multi) throws IOException {
		//String jsonString = Document.toJsonString(query);
		File inputFile = collectionFile;
		File tempFile = new File("myTempFile.txt");

		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

		String lineToRemove = Document.toJsonString(query);
		String currentLine;
		boolean stop = false;
			while((currentLine = reader.readLine()) != null) {
			    // trim newline when comparing with lineToRemove
			    String trimmedLine = currentLine.trim();
			    trimmedLine = trimmedLine.replaceAll("\\s+", "");
			    if(trimmedLine.equals("")) {
			    	continue;
			    }
			    if(trimmedLine.equals(lineToRemove) && !stop) {
			    	if(multi) {
			    		continue;
			    	} else {
			    		stop = true;
			    		continue;
			    	}
			    }
			    writer.write(currentLine);
			    writer.write("\n");
			    writer.write("\t");
			    writer.write("\n");
			    //writer.flush();
			}
		
		writer.close(); 
		reader.close(); 
		boolean successful = tempFile.renameTo(inputFile);
	}

	/**
	 * Returns the number of documents in this collection
	 */
	public long count() {
		int res = 0;
		DBCursor cursor = find();
		return cursor.count();
	}

	public String getName() {
		return this.collectionName;
	}

	/**
	 * Returns the ith document in the collection. Documents are separated by a line
	 * that contains only a single tab (\t) Use the parse function from the document
	 * class to create the document object
	 */
	
	
	//ref about reding
//	https://stackoverflow.com/questions/7463414/what-s-the-best-way-to-load-a-jsonobject-from-a-json-text-file
//	https://stackoverflow.com/questions/2308188/getresourceasstream-vs-fileinputstream/
	public JsonObject getDocument(int i) {
        // should be moved into DB cursor?		
		FileInputStream IS;
		try {
			IS = new FileInputStream(collectionFile);
			InputStreamReader ISR = new InputStreamReader(IS);
	        BufferedReader reader = new BufferedReader(ISR);
			int j =0;
			String res = "";
	        while(reader.ready() && j <= i) {
	            String line = reader.readLine();
		        System.out.println("line " + line);
		        System.out.println(line.equals("\t"));

	            if(j == i) {
	            	res += line;
	            }

	            if (line.equals("\t")) {
	            	if(j == i) {
	            		System.out.println("building " + res);
				        JsonObject resJson = Document.parse(res);
				        return resJson;
	            	}		    
			        res = "";
			        j++;  
	            }
	        }

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


        return new JsonObject();

			
	
	}

	/**
	 * Drops this collection, removing all of the documents it contains from the DB
	 */
	public void drop() {
		this.collectionName = null;
		//this.documents = null;
		//this.collectionFile = null;
		Path dir =  Paths.get(this.filePath);
		try {
			Files.delete(dir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.collectionFile = null;
		this.collectionName = null;

	}

}