package hw5;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;

public class DB {

	/**
	 * Creates a database object with the given name.
	 * The name of the database will be used to locate
	 * where the collections for that database are stored.
	 * For example if my database is called "library",
	 * I would expect all collections for that database to
	 * be in a directory called "library".
	 * 
	 * If the given database does not exist, it should be
	 * created.
	 */
	
	private String path = "testfiles/";
	private String name;
	private File DBDir;
	private HashMap<String, DBCollection> collections;
	
	public DB(String name) {
		this.name=name;
		this.DBDir = new File(path + name);	 
		
		if (!DBDir.exists()) {
		    try{
		    	DBDir.mkdir();
		    } 
		    catch(SecurityException se){
		        //handle it
		    }        
		}

		
	}
	
	/**
	 * Retrieves the collection with the given name
	 * from this database. The collection should be in
	 * a single file in the directory for this database.
	 * 
	 * Note that it is not necessary to read any data from
	 * disk at this time. Those methods are in DBCollection.
	 */
	public DBCollection getCollection(String name) {
		
		File[] files = DBDir.listFiles();
		for (int i = 0; i < files.length; i++) {
			  if (files[i].isFile()) {
			    System.out.println("File " + files[i].getName());
			    System.out.println("looking for " + name);

			    if (files[i].getName().equals(name + ".json")) {
				    System.out.println("match " );
				    //return collections.get(name);
			    }
			    
			  } 
		}
		    
		DBCollection collection;
		collection = new DBCollection(this, name);
		//this.collections.put(name, collection);
//		if (collections.containsKey(name)) {
//			return collections.get(name);
//		}
		
		return collection;
	}
	
	/**
	 * Drops this database and all collections that it contains
	 */
	public void dropDatabase() throws IOException{
		deleteFiles(this.DBDir);
		this.collections = null;
		this.name = null;
	}
	
	
	/**
	 * helper function to delete dir recursively
	 */
	public void deleteFiles(File f) throws IOException{
		
	  if (f.isDirectory()) {
		    File[] files = f.listFiles();
		      for (File entry : files) {
		    	  deleteFiles(entry);
		    }
		  }
	  if (!f.delete()) {
	    throw new IOException("Failed to delete " + f);
	  }
	}
	
	
	public File getPath() {
		return this.DBDir;
	}
	
	
}
