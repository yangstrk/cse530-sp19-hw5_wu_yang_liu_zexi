package hw5;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;



//jsonelment generic
//json array, list of object
//json object
//json primitive


public class DBCursor implements Iterator<JsonObject>{
	

	
	// and object that allows access to query results
	// like an iterator
	//private DBCollection collection;
	private ArrayList<JsonObject> documents;
	private JsonObject query;
	private JsonObject fields;
	private Iterator<JsonObject> iterator;
	//private Iterator<JsonObject> iterator = this.documents.iterator();

	// query it self inside of cursor class
//	* Queries that include all documents from the collection
//	db.users.find( {} )
//	* Queries that request a single document from the collection
//	db.mycol.find({"by":"tutorials point"})
//	db.mycol.find({"likes":{$lt:50}}).pretty()
//	* Queries based on data in an embedded document or list
//	* Queries with comparison operators (you do not have to implement the other operator types) 
	
	
//	eg1
//	 { <field>: <value> } 
//	db.mycol.find({"likes":{$lt:50}}).pretty()
//fields = likes
//query.get(fields.getAsString()) => $lt:50

//	eg2
	// Q could it be arrays?
//	{ "_id" : "1234", "versions" : [ { "owner_id" : "100000" }, { "owner_id" : "100001" }, { "owner_id" : "100001" } ] }
	
	public DBCursor(DBCollection collection, JsonObject query, JsonObject fields) {
		// if find all
		this.documents = new ArrayList<JsonObject>();
		
		if (query== null || query.toString()=="{}") {
			FileInputStream IS;
			try {
				IS = new FileInputStream(collection.getCollection());
				InputStreamReader ISR = new InputStreamReader(IS);
		        BufferedReader reader = new BufferedReader(ISR);
				String res = "";
		        while(reader.ready()) {
		            String line = reader.readLine();
		            if (line.equals("\t")) {
				        System.out.println("building " + res);
				        JsonObject resJson = Document.parse(res);
		            	this.documents.add(resJson);
				        res = "";
		            } else {
		            	 res += line;
		            }
		        }
		        
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.iterator = documents.iterator();
			return;	
		}		
		
		//find with a certain query
	
		if(query != null && fields == null) {
			System.out.println("query---- " );
			DBCursor cursor = collection.find();
			for (int i = 0; i < cursor.count(); i++) {
				boolean add = true;
				JsonObject tempJsonObject = cursor.documents.get(i);
				//System.out.println("JsonObject " + tempJsonObject);
				//System.out.println("query " + query);
				if(tempJsonObject.equals(query)) {
					this.documents.add(tempJsonObject);
					continue;
				}
				//get field of the query
				for(String Qfield: query.keySet()) {
//					System.out.println("key " + Qfield);
					String[] splitStrings = Qfield.split("\\.");
					JsonElement val = query.get(Qfield);
					System.out.println("obj "+tempJsonObject);

					if(splitStrings.length >= 2) {
						JsonObject cp = tempJsonObject;
						for (int k=0;k<splitStrings.length; k++) {
							JsonElement child = null;
							System.out.println("cp "+cp);

							child = cp.get(splitStrings[k]);
							System.out.println("key "+splitStrings[k]);
							System.out.println("child "+child);

							// if object does not have current key
							if(child == null) {
								System.out.println("true? "+child);
								add = false;
								continue;
							}
							
							if(child.isJsonPrimitive()) {
								if (k == splitStrings.length-1) {
									System.out.println("last "+ child);
									System.out.println("val "+ val);
									if(compareJson(val, child)) {
										continue;
									} else {
										add = false;
										continue;
									}
								}
							}
							else {  cp = (JsonObject) child;  }
						}
						
					}
					
					
					if (splitStrings.length ==1) {
						System.out.println("no nesting");
						//JsonElement val = query.get(Qfield);
						
						if (tempJsonObject.get(Qfield)==null) {
							add = false;
							continue;
						}

						JsonElement documentField = tempJsonObject.get(Qfield);
						if(compareJson(val, documentField)) {
							continue;
						} else if(!compareJson(val, documentField) && val.isJsonArray()) {
							add = false;
							continue;
						}
						
						String documentFieldStr = documentField.toString();
						if(!val.isJsonObject()) {  val = val.getAsJsonObject();  }
						if (val.isJsonObject()) {
							JsonObject obj = val.getAsJsonObject();
							Boolean canAddForCurrentQueryField = this.canAddDocument(documentFieldStr, obj);
							//System.out.println("canAddForCurrentQueryField " + canAddForCurrentQueryField);
							if (!canAddForCurrentQueryField) {  add = false;  }
						}
						
					}
					

					
					
		
					
				}
				if (add) {
					
					this.documents.add(tempJsonObject);
				
					/*else {
						
						for (String theOnlyKey: fields.keySet()) {
							JsonElement f = fields.get(theOnlyKey);
							JsonObject newObj = new JsonObject();
							System.out.println(f);
							HashSet<String> pool = new HashSet<>();
	
							if (f.isJsonArray()) {
								JsonArray fieldArr = f.getAsJsonArray();
								for(JsonElement e: fieldArr) {  pool.add(e.getAsString());  }
								for(String key: tempJsonObject.keySet()) {
									if (pool.contains(key)) {
										newObj.add(key, tempJsonObject.get(key));
									}
								}
							}
							else {
								for(String key: tempJsonObject.keySet()) {
									if (key.equals(f.getAsString())) {
										newObj.add(key, tempJsonObject.get(key));
									}
								}
							}
							this.documents.add(newObj);
							break;
						}
						
					}*/
					
				}
				
			}
			this.iterator = documents.iterator();
			return;
		}
		
		if(query != null && fields != null) {
			DBCursor cursor = collection.find(query);
			for(int i = 0; i < cursor.documents.size(); i++) {
				JsonObject jsonObject = cursor.documents.get(i);
				JsonObject resObject = new JsonObject();
				for(String field : fields.keySet()) {
					if(jsonObject.get(field) != null) {
						resObject.add(field, jsonObject.get(field));
					}
				}
				documents.add(resObject);
			}
			this.iterator = documents.iterator();
			return;
		}
		//
		
	}

	public boolean canAddDocument(String documentFieldStr,JsonObject obj) {
		boolean flag = false;
		
		for (String op: obj.keySet()) {
			JsonElement target = obj.get(op);
			String targetStr = target.toString();
			if (op.equals("$eq")) {
				if(documentFieldStr.equals(targetStr)) {
					System.out.println("hihihi" + documentFieldStr);
					flag = true;
				}
			}
			else if (op.equals("$gt")) {
				if(documentFieldStr.compareTo(targetStr) > 0) {	flag = true;	}
			}
			
			else if (op.equals("$gte")) {
				if(documentFieldStr.compareTo(targetStr) >= 0) {  flag =true;	}
			}
			else if (op.equals("$lt")) {
				if(documentFieldStr.compareTo(targetStr) < 0) {
					flag =true;				}
			}
			else if (op.equals("$lte")) {
				if(documentFieldStr.compareTo(targetStr) <= 0) {
					flag =true;				}
			}
			else if (op.equals("$ne")) {
				if(!documentFieldStr.equals(targetStr)) {
					flag =true;				}
			}
			else if (op.equals("$in")) {
				boolean addDocument =false;
				for(JsonElement o: target.getAsJsonArray()) {
					if(documentFieldStr.equals(o.toString())) {
						addDocument= true;
					}
				}
				if (addDocument) {  flag =true;  }
			}
			
			else if (op.equals("$nin")) {
				boolean addDocument = true;
				for(JsonElement o: target.getAsJsonArray()) {
					if(documentFieldStr.equals(o.toString())) {
						addDocument= false;
					}
				}
				if (addDocument) {  flag =true;  }
			}
		}
		
		System.out.println("helper return " + flag);
		return flag;

	}
	public static boolean compareJson(JsonElement jsonElement1, JsonElement jsonElement2) {
	    boolean isEqual = true;
	    // Check whether both jsonElement are not null
	    if (jsonElement1 != null && jsonElement2 != null) {

	      // Check whether both jsonElement are objects
	      if (jsonElement1.isJsonObject() && jsonElement2.isJsonObject()) {
	        Set<Entry<String, JsonElement>> ens1 = ((JsonObject) jsonElement1).entrySet();
	        Set<Entry<String, JsonElement>> ens2 = ((JsonObject) jsonElement2).entrySet();
	        JsonObject json2obj = (JsonObject) jsonElement2;
	        if (ens1 != null && ens2 != null && (ens2.size() == ens1.size())) {
	          // Iterate JSON Elements with Key values
	          for (Entry<String, JsonElement> en : ens1) {
	            isEqual = isEqual && compareJson(en.getValue(), json2obj.get(en.getKey()));
	          }
	        } else {
	          return false;
	        }
	      }

	      // Check whether both jsonElement are arrays
	      else if (jsonElement1.isJsonArray() && jsonElement2.isJsonArray()) {
	          JsonArray jarr1 = jsonElement1.getAsJsonArray();
	          JsonArray jarr2 = jsonElement2.getAsJsonArray();
	          if (jarr1.size() != jarr2.size()) {
	            return false;
	          } else {
	            int i = 0;
	            // Iterate JSON Array to JSON Elements
	            for (JsonElement je : jarr1) {
	              isEqual = isEqual && compareJson(je, jarr2.get(i));
	              i++;
	            }
	          }
	        }

	      // Check whether both jsonElement are null
	      else if (jsonElement1.isJsonNull() && jsonElement2.isJsonNull()) {
	        return true;
	      }

	      // Check whether both jsonElement are primitives
	      else if (jsonElement1.isJsonPrimitive() && jsonElement2.isJsonPrimitive()) {
	        if (jsonElement1.equals(jsonElement2)) {
	          return true;
	        } else {
	          return false;
	        }
	      } else {
	        return false;
	      }
	    } else if (jsonElement1 == null && jsonElement2 == null) {
	      return true;
	    } else {
	      return false;
	    }
	    return isEqual;

	  }
	
	
	
	public ArrayList<JsonObject> aggregate(DBCollection collection, JsonObject query, String fields) {
		ArrayList<JsonObject> results = new ArrayList<>();
		
		//find with a certain query
		if(query != null && fields == null) {
			DBCursor cursor = collection.find();
			for (int i = 0; i < cursor.count(); i++) {
				JsonObject tempJsonObject = cursor.documents.get(i);
				
				if(tempJsonObject.equals(query)) {
					this.documents.add(tempJsonObject);
				}
				
			}
			this.iterator = documents.iterator();
		}
		
		
		return results;
	}
	
	
	
	//get k,v from a json primitive
	public String getKey(JsonObject p) {
		String key = query.toString();
		return "hi";

//		for (String k: p.keySet()) {
//			System.out.println("key"+k);
//			return k.toString();
//		}
//		return key;
	}
	public String getVal(JsonObject p) {
		String key = query.toString();

		for (String k: p.keySet()) {
			return p.get(k).toString();
		}
		return key;
	}
	
	public void parse(JsonObject query) {
		String q = query.toString();
		
		
	}
	
	/**
	 * Returns true if there are more documents to be seen
	 */
	public boolean hasNext() {
		return this.iterator.hasNext();
	}

	/**
	 * Returns the next document
	 */
	public JsonObject next(){
		
		try {
			if(this.iterator.hasNext()) {
				return this.iterator.next();
			} else {
				return null;
			}

		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Returns the total number of documents
	 */
	public long count() {
		return documents.size();
	}

}
