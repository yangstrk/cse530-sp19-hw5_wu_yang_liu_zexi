package hw5;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.google.gson.*;

public class Document {
	
	// json objects from gson library = documents
	
	// https://static.javadoc.io/com.google.code.gson/gson/2.8.5/com/google/gson/JsonObject.html
	
	// https://static.javadoc.io/com.google.code.gson/gson/2.8.5/com/google/gson/JsonParser.html
	
	
	
	
	
	
	/**
	 * Parses the given json string and returns a JsonObject
	 * This method should be used to convert text data from
	 * a file into an object that can be manipulated.
	 */
	public static JsonObject parse(String json) {
		
		
//		JsonObject object = new JsonObject();
		
		
		JsonParser parser = new JsonParser();
		JsonObject object = (JsonObject) parser.parse(json);
//		JsonElement element = parser.parse(json);
//		if (element.isJsonArray()) {
//			return (JsonArray) element;
//		}
		
		// JsonPrimitive JsonObject JsonArray
		// string 		document	array
		
		return object;
	}
	
	
	
	public static JsonArray parseToJsonArray(String json) {
		
		
//		JsonObject object = new JsonObject();
		
		JsonParser parser = new JsonParser();
		JsonArray jsonArray = (JsonArray) parser.parse(json);
//		JsonElement element = parser.parse(json);
//		if (element.isJsonArray()) {
//			return (JsonArray) element;
//		}
		
		// JsonPrimitive JsonObject JsonArray
		// string 		document	array
		
		return jsonArray;
	}
	
	
	
	/**
	 * Takes the given object and converts it into a
	 * properly formatted json string. This method should
	 * be used to convert JsonObjects to strings
	 * when writing data to disk.
	 */
	public static String toJsonString(JsonObject json) {
		String string;
		string = json.toString();
		return string;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
